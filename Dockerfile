FROM caddy:2.6.2-builder-alpine AS builder

RUN xcaddy build \
    --with github.com/ueffel/caddy-brotli

FROM caddy:2.6.2-alpine

COPY --from=builder /usr/bin/caddy /usr/bin/caddy

RUN apk add --no-cache perl-utils xxd gettext curl

COPY Caddyfile /etc/caddy/Caddyfile
COPY entrypoint.sh /docker-entrypoint.sh
COPY envsubst-variables-entrypoint.sh /docker-entrypoint.d/50-envsubst-variables.sh
COPY patch-context-url-entrypoint.sh /docker-entrypoint.d/51-patch-context-url.sh
COPY patch-integrity-hashes-entrypoint.sh /docker-entrypoint.d/52-patch-integrity-hashes.sh
COPY startup-probe.sh /

RUN rm -rf /usr/share/caddy/html/*

ENV DUMMY_URL=http://dummy-url.tld
ENV CONTEXT_URL=/
ENV PATCH_INTEGRITY_BITS=384
ENV PROBE_TARGET_SCRIPT=""

WORKDIR /

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["caddy", "run", "--config", "/etc/caddy/Caddyfile", "--adapter", "caddyfile"]