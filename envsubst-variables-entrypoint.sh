#!/usr/bin/env sh
# vim: set ts=2 sts=2 sw=2 et number:

# find files with variables
variableFiles=$(grep -lrE "$(env | sed -E "s/(\w+)=.*/\\\\$\\\\{\1\\\\}/g" | tr "\n" "|" | sed 's/|$//')" /usr/share/caddy/html)

# replace variables in files
for f in ${variableFiles}; do
  envsubst "$(env | sed -E "s/(\w+)=.*/\${\1}/g" | tr "\n" "," | sed 's/,$//')" < "$f" > "${f}.tmp" && mv "${f}.tmp" "$f"
done
