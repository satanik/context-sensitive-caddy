#!/usr/bin/env sh
# vim: set ts=2 sts=2 sw=2 et number:

# normalize context url and delimit without a trailing slash
contextURL=$(echo "${CONTEXT_URL}" | sed -E 's@(://)|/$|((/)/+)@\1\3@g')

# find all files that contain links or scripts
integrityFiles=$(grep -lrE '<(link|script)[^>]*(src|href)="?'"${contextURL}"'([^"> ]+)"?([^>]*)integrity="?sha[0-9]+-([^"> ]+)"?[^>]*>' /usr/share/caddy/html)

# then loop them
for f in ${integrityFiles}; do
  # find all the asset URLs that need a new integrity hash
  # how it works:
  #   usually sed does not allow matching multiple patterns on the same line
  #   that is because the asterisk/star-operator is greedy (and no non-greedy alternative exists)
  #   therefore it can be utilised that tags start and end with angle-brackets
  #
  #  sed-parameters and flags used:
  #
  #   -E - extended regex
  #   -n - do not print
  #   p  - print
  #   g  - global
  #
  # steps:
  # 1.) match beginning of tag followed by any characters - not being the end
  # 2.) find src or href attributes
  # 3.) match potential quotes (not always present)
  # 4.) match the page url (is not needed and will be discarded)
  # 5.) match the relative url of the asset
  # 6.) match everything until an integrity attribute is found, that also contains a hash
  # 7.) match the end of a tag
  # 8.) replace everything match (whole tag) with only the relative url prefexed by a matchable string
  #     and pre- and suffixed by a line break, to put it on its own line
  # 9.) use awk to match matchable string and return second columen - containing the relative url
  urls=$(sed -nE 's@<(link|script)[^>]*(src|href)="?'"${contextURL}"'([^"> ]+)"?([^>]*)integrity="?sha[0-9]+-([^"> ]+)"?[^>]*>@\nURL: \3\n@gp' "$f" | awk '/URL:/ { print $2 }' | uniq)

  # loop all the relative URLs found in the current file
  for url in ${urls}; do
    if ! test -f "/usr/share/caddy/html${url}"; then
      break
    fi
    # use the relative URL to get a sha256 hash of the newly patched file
    integrity=$(shasum -a ${PATCH_INTEGRITY_BITS} "/usr/share/caddy/html${url}" | awk '{ print $1 }' | xxd -r -p | base64)
    # replace the old integrity hash value with the new one
    sed -i -E 's@('"${url}"'"?)([^i>]*)(integrity="?[^"> ]*"?)?@\1 integrity="sha'${PATCH_INTEGRITY_BITS}'-'"${integrity}"'"\2@g' "$f"
  done
done
