#!/usr/bin/env sh
# vim: set ts=2 sts=2 sw=2 et number:

set -eu

statuscode=$(curl --silent --output /dev/null --write-out "%{http_code}" http://localhost)
test $statuscode -eq 200

if [ ${PROBE_TARGET_SCRIPT} ]; then
  script=$(ls /usr/share/caddy/html | grep -e "${PROBE_TARGET_SCRIPT}")
  integrity=$(sed -E 's@'${script}'[^i]*integrity="?sha'${PATCH_INTEGRITY_BITS}'-([^"> ]*)"?@\nDGST: \1\n@g' /usr/share/caddy/html/index.html | awk '/DGST:/ { print $2 }')
  if [ ${integrity} ]; then
    echo $integrity | base64 -d | xxd -p | awk '{ printf "%s", $0 } END { print "  /usr/share/caddy/html/'$script'" }' | shasum -a ${PATCH_INTEGRITY_BITS} -c - > /dev/null
  fi 
fi
